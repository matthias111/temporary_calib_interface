﻿#include <string>

#include <ros/ros.h>
#include <ros/console.h>
#include <opencv2/core.hpp>
#include <opencv2/calib3d.hpp>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <tf2/LinearMath/Transform.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <visualization_msgs/Marker.h>

#include <calibration_msgs/observation.h>
#include <calibration_msgs/image_point.h>
#include <calibration_msgs/image_checks.h>
#include <calibration_msgs/accuracy_report.h>
#include <calibration_msgs/calibration_report.h>
#include <calibration_msgs/calibration_status.h>


void drawText( cv::InputOutputArray img, const cv::String& text, cv::Point &org,
               int fontFace, double fontScale, cv::Scalar color,
               int thickness = 1, int lineType = cv::LINE_8,
               bool bottomLeftOrigin = false, cv::Scalar bg_color = cv::Scalar(127,127,127),
               int buffer = 10, bool allign_center = false){
    if (text.empty())
        return;

    std::stringstream stream(text);
    std::string line;

    while(std::getline(stream,line,'\n')){
        int baseline = 0;
        cv::Size textSize = cv::getTextSize(line, fontFace, fontScale, thickness, &baseline);
        cv::Point temp = (allign_center) ? cv::Point(org.x-textSize.width/2+buffer, org.y) : org;
        cv::Point p1 = temp + cv::Point(-buffer, buffer);
        cv::Point p2 = temp + cv::Point(textSize.width+buffer*2, -textSize.height-buffer*2);
        cv::rectangle(img, p1, p2 ,bg_color, -1);
        cv::putText(img, line, temp, fontFace, fontScale, color, thickness, lineType, bottomLeftOrigin);

        org += cv::Point(0,textSize.height+buffer*2); // return for next line
    }
}

class calibration_view
{
    ros::NodeHandle nh;
    image_transport::ImageTransport transport;
    std::string m_camera_name_left, m_camera_name_right;
    image_transport::Publisher pub_detected_obs;
    image_transport::Subscriber image_sub ;

    // last accuracy report
    ros::Subscriber sub_acc_report;
    calibration_msgs::accuracy_report acc_report;

    // last calibration report
    ros::Subscriber sub_cal_report;
    calibration_msgs::calibration_report cal_report;

    // image checks
    ros::Subscriber sub_img_checks;
    calibration_msgs::image_checks img_checks;

    // calibration status
    ros::Subscriber sub_status;
    calibration_msgs::calibration_status status;

    // observations
    ros::Subscriber sub_obs;
    calibration_msgs::observation observations;

public:
    calibration_view(std::string camera_name_left,
                     std::string camera_name_right) :
        transport(nh),
        m_camera_name_left(camera_name_left),
        m_camera_name_right(camera_name_right)
    {
        pub_detected_obs = transport.advertise("vis_detected_obs", 10);

        image_sub = transport.subscribe(m_camera_name_left + "/image_raw", 1, &calibration_view::callback_detected_checks, this);

        sub_acc_report = nh.subscribe("accuracy_report", 1, &calibration_view::callback_acc_report, this);
        sub_cal_report = nh.subscribe("calibration_report", 1, &calibration_view::callback_cal_report, this);
        sub_img_checks = nh.subscribe("calib_image_checks", 1, &calibration_view::callback_img_checks, this);
        sub_status = nh.subscribe("calib_status", 1, &calibration_view::callback_status, this);
        sub_obs = nh.subscribe("calib_detected_obs", 1, &calibration_view::callback_obs, this);
    }

    void callback_cal_report(const calibration_msgs::calibration_reportConstPtr &report){
        cal_report = *report;
    }

    void callback_acc_report(const calibration_msgs::accuracy_reportConstPtr &report){
        acc_report = *report;
    }

    void callback_img_checks(const calibration_msgs::image_checksConstPtr &checks){
        img_checks = *checks;
    }

    void callback_status(const calibration_msgs::calibration_statusPtr &s){
        status = *s;
    }

    void callback_obs(const calibration_msgs::observationConstPtr &o){
        observations = *o;
    }

    void callback_detected_checks(const sensor_msgs::ImageConstPtr &image_l)
    {
        //        double t_start = (double)cv::getTickCount();

        cv::Scalar color_red = cv::Scalar(255,0,0);
        cv::Scalar color_green = cv::Scalar(0,255,0);
        cv::Scalar color_blue = cv::Scalar(0,0,255);
        cv::Scalar color_yellow = cv::Scalar(255,255,0);

        if (image_l->encoding == sensor_msgs::image_encodings::BGR8){
            color_red = cv::Scalar(0,0,255);
            color_green = cv::Scalar(0,255,0);
            color_blue = cv::Scalar(255,0,0);
            color_yellow = cv::Scalar(0,255,255);
        }
        
        // Draw markers to debugging image
        cv::Mat debugging_image;
        try {
            if (image_l->encoding == "mono8") {
                cv::cvtColor(cv_bridge::toCvCopy(image_l)->image,debugging_image,cv::COLOR_GRAY2RGB);
            } else {
                debugging_image = cv_bridge::toCvCopy(image_l)->image;
            }
        }
        catch (cv_bridge::Exception& e) {
            ROS_ERROR("cv_bridge exception: %s", e.what());
            return;
        }

        // Variables
        cv::Point center = debugging_image.size()/2;
        int font = cv::FONT_HERSHEY_PLAIN;
        int fontScale = double(debugging_image.rows)*0.0035;
        std::stringstream stream;
        stream << std::fixed << std::setprecision(2);
        cv::Point pos;
        cv::Scalar color_value = color_red;
        cv::Scalar color_value_font = cv::Scalar(255,255,255);
        std::string message;
        bool obs_detected = abs(observations.header.stamp.toSec()-image_l->header.stamp.toSec()) < 1.0;
        bool testfield_visible = obs_detected && observations.observed_points.size() > 5;

        // General message
        if (!testfield_visible){
            message = "Test field not detected.";
        }

        // Render area outside of calib field red
        if (testfield_visible && status.status != status.STANDBY && img_checks.field_edge.size()>5){
            cv::Mat mask = cv::Mat::zeros(debugging_image.size(), CV_8UC1);
            std::vector<std::vector<cv::Point>> corners = {{}};
            for (unsigned int i = 0; i < img_checks.field_edge.size(); i++){
                corners[0].push_back(cv::Point(img_checks.field_edge.at(i).x,img_checks.field_edge.at(i).y));
            }
            cv::fillPoly(mask, corners, cv::Scalar(255), cv::LINE_8 );
            cv::Mat red = cv::Mat(debugging_image.size(), CV_8UC3, color_red);
            cv::copyTo(debugging_image, red, mask);
            cv::addWeighted( debugging_image, 0.4, red, 0.6, 0.0, debugging_image);
        }

        // Detected points
        if (obs_detected){
            for (auto const &p: observations.observed_points){
                if (p.id > 0){
                    cv::drawMarker(debugging_image,cv::Point2d(p.x,p.y),cv::Scalar(0,255,0),cv::MARKER_CROSS,60,8);
                    cv::putText(debugging_image, std::to_string(p.id), cv::Point(p.x, p.y), font, 4, color_red, 3);
                } else {
                    cv::drawMarker(debugging_image,cv::Point2d(p.x,p.y),cv::Scalar(0,255,0),cv::MARKER_CROSS,30,4);
                }
            }
        }

        // Start position
        pos = cv::Point(5,5);
        drawText(debugging_image, " ", pos, font, fontScale*0.5, color_value, 6 ,cv::LINE_8);

        //FPS
        std::string fps_string = (img_checks.fps>0) ? std::to_string(int(img_checks.fps)) : "-";
        drawText(debugging_image, "FPS: " + fps_string, pos, font, fontScale*0.5, cv::Scalar(255,255,255), 3 ,cv::LINE_8);

        // Print curent mode
        cv::Point pos_center = cv::Point(center.x, pos.y);
        std::string mode_text = "Error";
        if (status.status == status.STANDBY)
            mode_text = "Standby";
        else if (status.status == status.DATA_CAPTURE)
            mode_text = "Capturing Images";
        else if (status.status == status.CHECKING_ACCURACY)
            mode_text = "Checking Accuracy";
        else if (status.status == status.RUNNING_CALIBRATION){
            mode_text = "Running Calibration...";
            message = "Please wait.";
        }

        if (img_checks.pos_id_number != img_checks.pos_id_current && !(status.status == status.STANDBY || status.status == status.RUNNING_CALIBRATION)){
            mode_text += " (" + std::to_string(observations.position_index) + "/" + std::to_string(observations.n_positions) + ")";
        }
        drawText(debugging_image, mode_text, pos_center, font, fontScale*1.25, cv::Scalar(255,255,255),7,cv::LINE_8,false, cv::Scalar(127,127,127),10,true);
        pos.y = pos_center.y;

        int radius_m = debugging_image.rows*0.15;
        if (testfield_visible){
            // Draw movement check
            color_value = (img_checks.movement.ok) ? color_green : color_red;
            stream.str("");
            stream << "Mov: " << int(round(img_checks.movement.current)) << " px";
            drawText(debugging_image, stream.str(), pos, font, fontScale, color_value, 6,cv::LINE_8);
            if (!img_checks.movement.ok){
                message = "Please keep camera steady.";
            }

            // Draw distance check (z)
            color_value = (img_checks.point_distance.ok) ? color_green : color_red;
            stream.str(std::string());
            stream << "Dist: " << (img_checks.point_distance.current-((img_checks.point_distance.max-img_checks.point_distance.min)/2+img_checks.point_distance.min))*100.0 << " cm";
            drawText(debugging_image, stream.str(), pos, font, fontScale, color_value, 6,cv::LINE_8, false, cv::Scalar(127,127,127));
            cv::circle(debugging_image, center, radius_m, cv::Scalar(100,100,100), 7);
            int radius_distance_check = int((double(radius_m)/(img_checks.point_distance.max - img_checks.point_distance.min))*img_checks.point_distance.current*0.5)+radius_m;
            if (radius_distance_check < 10)
                radius_distance_check = 10;
            cv::circle(debugging_image, center, radius_distance_check, color_value, 15);
            if (!img_checks.point_distance.ok){
                if (img_checks.point_distance.current < img_checks.point_distance.min)
                    message = "Please increase distance to\ncalibration field.";
                else
                    message = "Please decrease distance to\ncalibration field.";
            }
        }

        if ((status.status == status.DATA_CAPTURE || status.status == status.DATA_PROCESSING || status.status == status.CHECKING_ACCURACY) && testfield_visible){
            stream.str(std::string());
            stream <<"-Target Pose -";
            drawText(debugging_image, stream.str(), pos, font, fontScale, cv::Scalar(255,255,255), 6,cv::LINE_8);

            // Calib field cover checks
            color_value = (img_checks.cover_left.ok) ? color_green : color_red;
            stream.str(std::string());
            stream << "Cover: " << int(round(img_checks.cover_left.current)) << " %";
            drawText(debugging_image, stream.str(), pos, font, fontScale, color_value, 6,cv::LINE_8);

            // Draw rotation check (text)
            color_value = (img_checks.rx.ok) ? color_green : color_red;
            stream.str(std::string());
            stream << "RX: " << int(round(img_checks.rx.current)) << " deg";
            drawText(debugging_image, stream.str(), pos, font, fontScale, color_value, 6,cv::LINE_8);
            color_value = (img_checks.ry.ok) ? color_green : color_red;
            stream.str(std::string());
            stream << "RY: " << int(round(img_checks.ry.current)) << " deg";
            drawText(debugging_image, stream.str(), pos, font, fontScale, color_value, 6,cv::LINE_8);

            // Draw z-rotation check
            color_value = (img_checks.rz.ok) ? color_green : color_red;
            stream.str(std::string());
            stream << "RZ: " << int(round(img_checks.rz.current)) << " deg";
            drawText(debugging_image, stream.str(), pos, font, fontScale, color_value, 6,cv::LINE_8);
            if ((!img_checks.rz.ok)){
                cv::ellipse(debugging_image, center, cv::Size(radius_m, radius_m),0,-90,img_checks.rz.current-90,color_value,30);
                int start_x = (img_checks.rz.current < 0) ? -5 : 5;
                cv::arrowedLine(debugging_image, center+cv::Point(start_x, -radius_m), center+cv::Point(0, -radius_m), color_value, 30, 8, 0, 20);
                message = "Rotate camera around optical axis.";
            }

            // ---- Draw target pose to image ---
            if (img_checks.target_image_points.size()>3){
                std::vector<cv::Point2d> image_points;
                for(auto const &ip : img_checks.target_image_points)
                    image_points.push_back(cv::Point2d(ip.x,ip.y));

                if (image_points.size()>3){
                    cv::line(debugging_image,image_points[0],image_points[1],color_yellow,10);
                    cv::line(debugging_image,image_points[1],image_points[2],color_yellow,10);
                    cv::line(debugging_image,image_points[2],image_points[3],color_yellow,10);
                    cv::line(debugging_image,image_points[3],image_points[0],color_yellow,10);
                    cv::line(debugging_image,image_points[0],image_points[2],color_yellow,15);
                    cv::line(debugging_image,image_points[1],image_points[3],color_yellow,15);

                    // Draw corners
                    cv::drawMarker(debugging_image, image_points[0], color_blue, cv::MARKER_SQUARE      ,40,20);
                    cv::drawMarker(debugging_image, image_points[1], color_blue, cv::MARKER_SQUARE      ,40,20);
                    cv::drawMarker(debugging_image, image_points[2], color_blue, cv::MARKER_SQUARE      ,40,20);
                    cv::drawMarker(debugging_image, image_points[3], color_blue, cv::MARKER_SQUARE      ,40,20);
                    cv::drawMarker(debugging_image, image_points[4], color_blue, cv::MARKER_SQUARE      ,40,25);

                    // field of view (corners to optical center)
                    //  cv::line(debugging_image,image_points[4],image_points[5],color_green,30); // optical axis
                    //  cv::line(debugging_image,image_points[0],image_points[5],color_yellow,15);
                    //  cv::line(debugging_image,image_points[1],image_points[5],color_yellow,15);
                    //  cv::line(debugging_image,image_points[2],image_points[5],color_yellow,15);
                    //  cv::line(debugging_image,image_points[3],image_points[5],color_yellow,15);
                    //  cv::drawMarker(debugging_image, image_points[5], color_blue, cv::MARKER_TILTED_CROSS,200,25);
                }
            }
        }

        if (status.status == status.STANDBY){
            if(acc_report.pre_parameter.size() > 0){
                drawText(debugging_image, "-Accuracy (current)-", pos, font, fontScale, cv::Scalar(255,255,255), 6,cv::LINE_8);
                stream << std::fixed << std::setprecision(3);

                for (auto const &_p : acc_report.pre_parameter){
                    color_value = (_p.ok) ? color_green : color_red;
                    stream.str(std::string());
                    stream << _p.name << ": " << _p.value << " mm";
                    drawText(debugging_image, stream.str(), pos, font, fontScale, color_value, 6,cv::LINE_8);
                }

                color_value_font = (acc_report.pre_accuracy_check_passed) ? color_green : color_red;
                message = acc_report.message;
            }

            if (acc_report.post_parameter.size() > 0){
                drawText(debugging_image, "-Accuracy (new)-", pos, font, fontScale, cv::Scalar(255,255,255), 6,cv::LINE_8);

                for (auto const &_p : acc_report.post_parameter){
                    color_value = (_p.ok) ? color_green : color_red;
                    stream.str(std::string());
                    stream << _p.name << ": " << _p.value << " mm";
                    drawText(debugging_image, stream.str(), pos, font, fontScale, color_value, 6,cv::LINE_8);
                }

                color_value_font = (acc_report.post_accuracy_check_passed) ? color_green : color_red;
                message = acc_report.message;
            }
        }

        // Calibration Report
        if (status.status == status.STANDBY && cal_report.name != ""){
            drawText(debugging_image, "-Calibration-", pos, font, fontScale, cv::Scalar(255,255,255), 6,cv::LINE_8);
            for (auto const &_p : cal_report.report_parameter){
                color_value = (_p.ok) ? color_green : color_red;
                stream.str(std::string());
                stream << _p.name << ": " << _p.value << " px";
                drawText(debugging_image, stream.str(), pos, font, fontScale, color_value, 6,cv::LINE_8);
            }
        }

        // Draw general message
        pos = cv::Point(center.x, debugging_image.rows-100);
        drawText(debugging_image, message, pos, font, fontScale, color_value_font, 6,cv::LINE_8,false, cv::Scalar(127,127,127),10,true);

        // Publish image with additional information
        pub_detected_obs.publish(cv_bridge::CvImage(std_msgs::Header(), image_l->encoding , debugging_image).toImageMsg());

        //        ROS_INFO("Callback time: %f ms", ((cv::getTickCount() - t_start)/cv::getTickFrequency())*1000.0);
    }
};

int main(int argc, char **argv)
{
    ros::init(argc, argv, "calib_view");
    std::string l_camera_name, r_camera_name;

    if (argc == 2){
        l_camera_name = argv[1];
        r_camera_name = "";
    } else if (argc == 3) {
        l_camera_name = argv[1];
        r_camera_name = argv[2];
    } else {
        ROS_ERROR("Use: calib_view <camera_name_left> <camera_name_right>");
        return -1;
    }
    calibration_view view(l_camera_name, r_camera_name);
    ros::spin();
    return 0;
}
